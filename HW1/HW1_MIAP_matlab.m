%% HW1_MIAP
%% Q1
clc;
clear;
img = imread("Brain_MRI.png");
imgGray = rgb2gray(img);
%% Q1 a
figure()
montage(img)
title("R and G and B");

figure()
montage({img,imgGray });
title("RGB and Gray");
%% Q1 b

f = img;%Read in color image
g = im2bw(f);%Convert color image to binary image
h = rgb2gray(f);%Convert color image to grayscale image
figure();
subplot(1,3,1);
imshow(f);
title('RGB picture');
subplot(1,3,2);
imshow(g);
title('Binary graph');
subplot(1,3,3);
imshow(h);
title('Grayscale image');

figure();
grayI1 = rgbtogray(img);
subplot(1 , 2 , 1)
imshow(grayI1);
title("my RGB to Gray function");
grayI2 = rgb2gray(img);
subplot(1 , 2 , 2)
imshow(grayI2);
title("matlab's RGB to Gray function");

%% Q1 c
imwrite(grayI1,'Q1_b_rgbtogray.png');
%% Q2
clc;
clear;
% with unit8
I = imread("Hist.tif");

J1 = histeq(I);
J2 = adapthisteq(I ,'NumTiles',[7 7],'ClipLimit',0.5);


figure();
subplot(1 , 3 , 1)
imshow(I)
xlabel("image")

subplot(1 , 3 , 2)
imshow(J1)
xlabel("after histeq")

subplot(1 , 3 , 3)
imshow(J2)
xlabel("after adaptive histeq")

figure();
subplot(1 , 3 , 1)
imhist(I)
title("Histogram original image")

subplot(1 , 3 , 2)
imhist(J1)
title("Histogram after histeq")

subplot(1 , 3 , 3)
imhist(J2)
title("Histogram after adaptive histeq")
%% Q2 with double
I = double(imread("Hist.tif"));

J1 = histeq(I);
J2 = adapthisteq(I ,'NumTiles',[7 7],'ClipLimit',0.5);


figure();
subplot(1 , 3 , 1)
imshow(I)
xlabel("image")

subplot(1 , 3 , 2)
imshow(J1)
xlabel("after histeq")

subplot(1 , 3 , 3)
imshow(J2)
xlabel("after adaptive histeq")

figure();
subplot(1 , 3 , 1)
imhist(I)
title("Histogram original image")

subplot(1 , 3 , 2)
imhist(J1)
title("Histogram after histeq")

subplot(1 , 3 , 3)
imhist(J2)
title("Histogram after adaptive histeq")

%% Q3
clc; 
clear;
img = imread("heart_ct.jpg");
% img = (img);
%%  Q3 salt and pepper
figure();
subplot(1 , 4 , 1)
imshow(img);
title("original image");

Nimg1 = imnoise(img,'salt & pepper',0.02);
subplot(1 , 4 , 2)
imshow(Nimg1);
title("var noise is 0.02");

Nimg2 = imnoise(img,'salt & pepper',0.05);
subplot(1 , 4 , 3)
imshow(Nimg2)
title("var noise is 0.05");

Nimg3 = imnoise(img,'salt & pepper',0.1);
subplot(1 , 4 , 4)
imshow(Nimg3)
title("var noise is 0.1");

h1 = ones(3)/9; %LPF
h2 = ones(5)/(5^2); %LPF
h3 = ones(7)/(7^2); %LPF

Fimg11 = imfilter(img,h1);
Fimg12 = imfilter(img,h2);
Fimg13 = imfilter(img,h3);
Fimg21 = imfilter(Nimg1,h1);
Fimg22 = imfilter(Nimg1,h2);
Fimg23 = imfilter(Nimg1,h3);
Fimg31 = imfilter(Nimg2,h1);
Fimg32 = imfilter(Nimg2,h2);
Fimg33 = imfilter(Nimg2,h3);
Fimg41 = imfilter(Nimg3,h1);
Fimg42 = imfilter(Nimg3,h2);
Fimg43 = imfilter(Nimg3,h3);

figure();
xlabel("salt and peper")
subplot(3 , 4 , 1);
imshow(Fimg11)
title("original h=3*3");

subplot(3 , 4 , 2);
imshow(Fimg21)
title("var=0.02 h=3*3");

subplot(3 , 4 , 3);
imshow(Fimg31)
title("var=0.05 h=3*3");

subplot(3 , 4 , 4);
imshow(Fimg41)
title("var=0.1 h=3*3");

subplot(3 , 4 , 5);
imshow(Fimg12)
title("original h=5*5");

subplot(3 , 4 , 6);
imshow(Fimg22)
title("var=0.02 h=5*5");

subplot(3 , 4 , 7);
imshow(Fimg32)
title("var=0.05 h=5*5");

subplot(3 , 4 , 8);
imshow(Fimg42)
title("var=0.1 h=5*5");

subplot(3 , 4 , 9);
imshow(Fimg13)
title("original h=7*7");

subplot(3 , 4 , 10);
imshow(Fimg23)
title("var=0.02 h=7*7");

subplot(3 , 4 , 11);
imshow(Fimg33)
title("var=0.05 h=7*7");

subplot(3 , 4 , 12);
imshow(Fimg43)
title("var=0.1 h=7*7");

%%  Q3 gausian
figure();
subplot(1 , 4 , 1)
imshow(img);
title("original image");

Nimg1 = imnoise(img,'gaussian',0,0.02);
subplot(1 , 4 , 2)
imshow(Nimg1);
title("var noise is 0.02");

Nimg2 = imnoise(img,'gaussian',0,0.05);
subplot(1 , 4 , 3)
imshow(Nimg2)
title("var noise is 0.05");

Nimg3 = imnoise(img,'gaussian',0,0.1);
subplot(1 , 4 , 4)
imshow(Nimg3)
title("var noise is 0.1");

h1 = ones(3)/9; %LPF
h2 = ones(5)/(5^2); %LPF
h3 = ones(7)/(7^2); %LPF

Fimg11 = imfilter(img,h1);
Fimg12 = imfilter(img,h2);
Fimg13 = imfilter(img,h3);
Fimg21 = imfilter(Nimg1,h1);
Fimg22 = imfilter(Nimg1,h2);
Fimg23 = imfilter(Nimg1,h3);
Fimg31 = imfilter(Nimg2,h1);
Fimg32 = imfilter(Nimg2,h2);
Fimg33 = imfilter(Nimg2,h3);
Fimg41 = imfilter(Nimg3,h1);
Fimg42 = imfilter(Nimg3,h2);
Fimg43 = imfilter(Nimg3,h3);

figure();
subplot(3 , 4 , 1);
imshow(Fimg11)
title("original h=3*3");

subplot(3 , 4 , 2);
imshow(Fimg21)
title("var=0.02 h=3*3");

subplot(3 , 4 , 3);
imshow(Fimg31)
title("var=0.05 h=3*3");

subplot(3 , 4 , 4);
imshow(Fimg41)
title("var=0.1 h=3*3");

subplot(3 , 4 , 5);
imshow(Fimg12)
title("original h=5*5");

subplot(3 , 4 , 6);
imshow(Fimg22)
title("var=0.02 h=5*5");

subplot(3 , 4 , 7);
imshow(Fimg32)
title("var=0.05 h=5*5");

subplot(3 , 4 , 8);
imshow(Fimg42)
title("var=0.1 h=5*5");

subplot(3 , 4 , 9);
imshow(Fimg13)
title("original h=7*7");

subplot(3 , 4 , 10);
imshow(Fimg23)
title("var=0.02 h=7*7");

subplot(3 , 4 , 11);
imshow(Fimg33)
title("var=0.05 h=7*7");

subplot(3 , 4 , 12);
imshow(Fimg43)
title("var=0.1 h=7*7");
%% Q4
clc;
clear;
I = imread("retina.png");
r = double(I)/255;              
c = 1;              

subplot(2,2,1);
imshow(uint8(I));
title('Original Image');

s2 = c*(r).^0.3; % power law
subplot(2,2,2);
imshow(s2);
title('PLT Image gamma = 0.3');

s3 = c*(r).^1.2; % power law
subplot(2,2,3);
imshow(s3);
title('PLT Image gamma = 1.2');

s4 = c*(r).^2; % power law
subplot(2,2,4);
imshow(s4);
title('PLT Image gamma = 2');






%% Q5
clc;
clear;
I = imread("retina.png");
I = im2double(I);


FI = medfilt2(I);
% imshow(FI);


newI = FI;
maxx = max(max(FI));
ther = 0.95*maxx;
for i = 1:length(FI(: , 1))
    for j = 1:length(FI(1 , :))
        if(FI(i , j)<ther)
            newI(i,j) = 0;
        end
    end
end
k = 0.5;
FinalI = k*(newI - FI) + FI;


figure()
subplot(1 , 4 , 1);
imshow(I)
title("original image");

subplot(1 , 4 , 2);
imshow(FI)
title("denoised");

subplot(1 , 4 , 3);
imshow(FinalI)
title("after subtraction");

subplot(1 , 4 , 4);
imshow(newI)
title("after threshhold");


%% functions

function [grayI] = rgbtogray(I)
    B=I(:, :, 3);
    R=I(:, :, 1);
    G=I(:, :, 2);
    [n, m, ~]=size(I);
    grayI=zeros(n, m, 'uint8');
    for i=1:n
        for j=1:m
           grayI(i, j)= R(i, j)*0.2989 + G(i, j)*0.5870 + B(i, j)*0.114;
        end
    end
end

