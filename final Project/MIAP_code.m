%% MIAP Project
% please note for finding the output we just need to run the files with
% thier name, and the code is complete otherwise.(form line 56- the hole cord)
%% part 1-1
% in this part I read files and plot them with the myPlot function


clc;
clear;
V = niftiread('pat1.nii');
M = niftiread('pat1_label.nii');
name = "pat 1";
myPlot(V, M, name);

V = niftiread('00.nii');
M = niftiread('00_mask.nii');
name = "Atlas 1";
myPlot(V, M, name);

V = niftiread('pat5.nii');
M = niftiread('pat5_label.nii');
name = "pat 5";
myPlot(V, M, name);

clc;
clear;
V = niftiread('pat15.nii');
M = niftiread('pat15_label.nii');
name = "pat 15";
myPlot(V, M, name);
%% part 1-2
% here I downsample and show the point clouds and their surfaces
n = 'pat10_label.nii';
Mask = niftiread(n);
Mask = myVolumeCorrector(Mask,niftiinfo(n));
[SL] = myHSurf(Mask);

figure();
pc1 = pointCloud(SL);
pc1 = pcdownsample(pc1,'random',1);
pcshow(pc1);

LL = myChanCor(Mask);
figure();
pc2 = pointCloud(LL);
pc2 = pcdownsample(pc2,'random',1);
pcshow(pc2);

figure();
pc2 = pcdownsample(pc2,'random',0.2);
pcshow(pc2);




%%  the hole cord
% registering the whole cord
clc;
clear;
n = 'pat631_label.nii';
Mask = niftiread(n);
Mask = myVolumeCorrector(Mask,niftiinfo(n));
[SL] = myHSurf(Mask);

AMask = niftiread('00_mask.nii');
AMask = myVolumeCorrector(AMask,niftiinfo('00_mask.nii'));
[ASL] = myHSurf(AMask);

[pc,Apc,mpc] = pdCPD(SL , ASL , 0.1);
%%
% finding the locations of registered image for interpolating and finding
% functions
a = pc.Location;
x1 = a(: , 1);
y1 = a(: , 2);
z1 = a(: , 3);

a = mpc.Location;
X1 = a(: , 1);
Y1 = a(: , 2);
Z1 = a(: , 3);

%% interpolation with griddata
% I interpolated data for finding needed parameters
m1 = griddata(x1,y1,z1,X1,SL(:,1),SL(:,2),SL(:,3));
n1 = griddata(x1,y1,z1,Y1,SL(:,1),SL(:,2),SL(:,3));
k1= griddata(x1,y1,z1,Z1,SL(:,1),SL(:,2),SL(:,3));

% removing the NaNs
% toremove = isnan(m1);
% m1(toremove) = [];
% n1(toremove) = [];
% k1(toremove) = [];
% toremove = isnan(n1);
% m1(toremove) = [];
% n1(toremove) = [];
% k1(toremove) = [];
% toremove = isnan(k1);
% m1(toremove) = [];
% n1(toremove) = [];
% k1(toremove) = [];

mmpc = [m1 n1 k1];
mmpc = pointCloud(mmpc);

figure()
pcshowpair(mpc,mmpc,'MarkerSize',50)
title('Point clouds after registration')
legend({'Moving point cloud','Fixed point cloud'},'TextColor','w')
legend('Location','southoutside')
xlabel('X');
ylabel('Y');
zlabel('Z');

%% Interpolating Transform to all of Points (tform)
% interpolating to find the tform matrix needed in other parts and Jaccob
% matrix
[tform,mpc] = pdCPDTrans(SL , ASL , 0.05);
Vx = tform(:,1);
Vy = tform(:,2);
Vz = tform(:,3);

lenn = length(Vx);
[Xq,Yq,Zq] = meshgrid(1:size(Mask,1),1:size(Mask,2),1:size(Mask,3));
dx = griddata(x1(1:lenn),y1(1:lenn),z1(1:lenn),Vx,Xq,Yq,Zq);
dy = griddata(x1(1:lenn),y1(1:lenn),z1(1:lenn),Vy,Xq,Yq,Zq);
dz= griddata(x1(1:lenn),y1(1:lenn),z1(1:lenn),Vz,Xq,Yq,Zq);
% here is how we use Jaccob
% out = myJaccob(dx,dy,dz);


%% interpolation for part 1(fit)
% this was my first approach for interpolation, I used simple fit function
fit1 = fit(x1,X1,'poly2');
fit2 = fit(y1,Y1,'poly2');
fit3 = fit(z1,Z1,'poly2');

m1 = fit1(x1);
m2 = fit2(y1);
m3 = fit3(z1);
mmpc = [m1 m2 m3];
mmpc = pointCloud(mmpc);

figure()
pcshowpair(mpc,mmpc,'MarkerSize',50)
title('Point clouds after registration')
legend({'Moving point cloud','Fixed point cloud'},'TextColor','w')
legend('Location','southoutside')
xlabel('X');
ylabel('Y');
zlabel('Z');



%% part three B
% in this part I have got the serface of data sepately for interpolation
% n = 'pat6_label.nii';
Mask = niftiread(n);
Mask = myVolumeCorrector(Mask,niftiinfo(n));
[SL1, SL2, SL3, SL4, SL5] = mySurf(Mask);

AMask = niftiread('00_mask.nii');
AMask = myVolumeCorrector(AMask,niftiinfo('00_mask.nii'));
[ASL1, ASL2, ASL3, ASL4, ASL5] = mySurf(AMask);

% [pc1,Apc1,mpc1] = pdCPD(SL1 , ASL1 , 0.1);
[pc2,Apc2,mpc2] = pdCPD(SL2 , ASL2 , 0.1);
[pc3,Apc3,mpc3] = pdCPD(SL3 , ASL3 , 0.1);
[pc4,Apc4,mpc4] = pdCPD(SL4 , ASL4 , 0.1);
% [pc5,Apc5,mpc5] = pdCPD(SL5 , ASL5 , 0.1);
%% finding the locations for interpolation
a = pc1.Location;
x1 = a(: , 1);
y1 = a(: , 2);
z1 = a(: , 3);
a = pc2.Location;
x2 = a(: , 1);
y2 = a(: , 2);
z2 = a(: , 3);
a = pc3.Location;
x3 = a(: , 1);
y3 = a(: , 2);
z3 = a(: , 3);
a = pc4.Location;
x4 = a(: , 1);
y4 = a(: , 2);
z4 = a(: , 3);
a = pc4.Location;
x5 = a(: , 1);
y5 = a(: , 2);
z5 = a(: , 3);

a = mpc1.Location;
X1 = a(: , 1);
Y1 = a(: , 2);
Z1 = a(: , 3);
a = mpc2.Location;
X2 = a(: , 1);
Y2 = a(: , 2);
Z2 = a(: , 3);
a = mpc3.Location;
X3 = a(: , 1);
Y3 = a(: , 2);
Z3 = a(: , 3);
a = mpc4.Location;
X4 = a(: , 1);
Y4 = a(: , 2);
Z4 = a(: , 3);
a = mpc4.Location;
X5 = a(: , 1);
Y5 = a(: , 2);
Z5 = a(: , 3);

x = [x1' x2' x3' x4' x5']';
y = [y1' y2' y3' y4' y5']';
z = [z1' z2' z3' z4' z5']';

X = [X1' X2' X3' X4' X5']';
Y = [Y1' Y2' Y3' Y4' Y5']';
Z = [Z1' Z2' Z3' Z4' Z5']';


%% interpolation with griddata
moving = pc.Location;
m1 = griddata(x,y,z,X,SL(:,1),SL(:,2),SL(:,3));
n1 = griddata(x,y,z,Y,SL(:,1),SL(:,2),SL(:,3));
k1= griddata(x,y,z,Z,SL(:,1),SL(:,2),SL(:,3));

mpc = [m1 n1 k1];
mpc = pointCloud(mpc);

figure()
pcshowpair(mpc,mpc,'MarkerSize',50)
title('Point clouds after registration')
legend({'Moving point cloud','Fixed point cloud'},'TextColor','w')
legend('Location','southoutside')
xlabel('X');
ylabel('Y');
zlabel('Z');

%% interpolation for part 2(fit)
fit1 = fit(x,X,'poly2');
fit2 = fit(y,Y,'poly2');
fit3 = fit(z,Z,'poly2');

m1 = fit1(x);
m2 = fit2(y);
m3 = fit3(z);
mmpc = [m1 m2 m3];
mmpc = pointCloud(mmpc);

% figure()
% pcshowpair(mpc,mpc,'MarkerSize',50)
% title('Point clouds after registration')
% legend({'Moving point cloud','Fixed point cloud'},'TextColor','w')
% legend('Location','southoutside')
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
%% the functions of part 4(evaluating the data, are given in function part)
% JJ = myJaccob(dx,dy,dz)
HD = HausdorffDist(mpc.Location, Apc.Location)
DSC = myDSC2(mpc.Location, Apc.Location)
ASD = myASD2(mpc.Location,Apc.Location)
overlappp = myOverlap2(mpc.Location,Apc.Location)
%%
function out = myChanCor(Mask)
    [xx , yy , zz] = size(Mask);
    pp2 = ~~Mask;
    a = sum(pp2 , 'all');
    point3n = zeros(a,3);
    count =0;
    for i = 1:xx
        for j = 1:yy
            for k = 1:zz
                if( pp2(i , j , k))
                    count = count+1;
                    point3n(count, 1)=i;
                    point3n(count, 2)=j;
                    point3n(count, 3)=k;
                end
            end
        end
    end
    out = point3n;
end

function [SL1, SL2, SL3, SL4, SL5] = mySurf(Mask)
    % input: the MASK 
    % out put is the serface points of L1:L5 in xyz Matrises
    [xx , yy , zz] = size(Mask);
    L5 = zeros(xx , yy , zz);
    L4 = zeros(xx , yy , zz);
    L3 = zeros(xx , yy , zz);
    L2 = zeros(xx , yy , zz);
    L1 = zeros(xx , yy , zz);
    for i = 1:xx
        for j = 1:yy
            for k = 1:zz
                if( Mask(i , j , k)==25)
                    L5(i , j , k)=1;
                end
                if( Mask(i , j , k)==24)
                    L4(i , j , k)=1;
                end            
                if( Mask(i , j , k)==23)
                    L3(i , j , k)=1;
                end
                if( Mask(i , j , k)==22)
                    L2(i , j , k)=1;
                end
                if( Mask(i , j , k)==21)
                    L1(i , j , k)=1;
                end            
            end
        end
    end

    CL5 = myChanCor(L5);
    CL4 = myChanCor(L4);
    CL3 = myChanCor(L3);
    CL2 = myChanCor(L2);
    CL1 = myChanCor(L1);
    


    % finding the surface points
    k = reshape(boundary(CL5,1),1,[]);
    k = unique(k);
    SL5 = CL5(k,:);
%     SL5 = SL5 - mean(SL5 , 1);

    k = reshape(boundary(CL4,1),1,[]);
    k = unique(k);
    SL4 = CL4(k,:); 
%     SL4 = SL4 - mean(SL4 , 1);
    
    k = reshape(boundary(CL3,1),1,[]);
    k = unique(k);
    SL3 = CL3(k,:);
%     SL3 = SL3 - mean(SL3 , 1);
    
    k = reshape(boundary(CL2,1),1,[]);
    k = unique(k);
    SL2 = CL2(k,:);
%     SL2 = SL2 - mean(SL2 , 1);
    
    k = reshape(boundary(CL1,1),1,[]);
    k = unique(k);
    SL1 = CL1(k,:);
%     SL1 = SL1 - mean(SL1 , 1);
end


function [SL] = myHSurf(Mask)
    % input: the MASK 
    % out put is the serface points of spinal cord in xyz Matrises
    [xx , yy , zz] = size(Mask);
    L = zeros(xx , yy , zz);

    for i = 1:xx
        for j = 1:yy
            for k = 1:zz
                if( Mask(i , j , k)<=25 &&  Mask(i , j , k)>=20)
                    L(i , j , k)=1;
                end          
            end
        end
    end

    CL = myChanCor(L);

    % finding the surface points
    k = reshape(boundary(CL,1),1,[]);
    k = unique(k);
    SL = CL(k,:);
    % subtracting the mean
    SL = SL - mean(SL , 1);
end

function out = myVolumeCorrector(Mask,info)
    %resizing for same volume
    S = size(Mask);
    a = info.PixelDimensions;
    out = imresize3(Mask,S.*a);
end


function out = myASD2(A,B)
    v1 = alphaShape(A(:,1), A(:,2), A(:,3)); 
    v2 = alphaShape(B(:,1), B(:,2), B(:,3));
    mmx = 0;
    for i=1:size(B,1)
        y = B(i,:);
        idx = nearestNeighbor(v1,y(1),y(2),y(3));
        tt = A(idx,:);
        ydist = norm(y-tt);
        %calculating the sum
        mmx = mmx+ydist;
    end

    mmy = 0;
    for i=1:size(A,1)
        x = A(i,:);
        idx = nearestNeighbor(v2,x(1),x(2),x(3));
        y_x = B(idx,:);
        x_dist = norm(x-y_x);
        mmy = mmy+x_dist;
    end
    out = (mmy+mmx)/(size(A,1)+size(B,1));
end

% there is two function for ASD, one work with matrix and the other works
% with location, I don't know which is wanted.
function out = myASD(A , B)
    C = (A-B).^2;
    out = mean(C,'all');
end

function [dist] = myHD(Mask1, Mask2) 
%     A = myChanCor(Mask1);
%     B = myChanCor(Mask2);
    m = size(A, 1); 
    n = size(B, 1); 
    dim= size(A, 2); 
    dist = zeros(m , 1);
    for k = 1:m 
        C = ones(n, 1) * A(k, :); 
        D = (C-B) .* (C-B); 
        D = abs(D * ones(dim,1)); % only for reducing the computations
        dist(k) = min(D); 
    end
    dist1 = max(dist);
    clear dist;
    dist = zeros(n,1);
    for k = 1:n 
        C = ones(m, 1) * B(k, :); 
        D = (C-A) .* (C-A); 
        D = abs(D * ones(dim,1)); % only for reducing the computations
        dist(k) = min(D); 
    end

    dist2 = max(dist);

    dist = sqrt(max(dist1,dist2));
end


function out = myDSC(refSegment , outSegment) 
     common = (refSegment & outSegment);
     a = sum(common(:));
     b = sum(refSegment (:));
     c = sum(outSegment(:));
     out = 2*a/(b+c);
end

function out = myDSC2(M, V)
    v1 = alphaShape(M(:,1), M(:,2), M(:,3)); 
    tf1 = inShape(v1,M(:,1), M(:,2), M(:,3));    
    v2 = alphaShape(V(:,1), V(:,2), V(:,3));
    tf2 = inShape(v2,V(:,1), V(:,2), V(:,3));
    tf_joint = inShape(v1,V(:,1), V(:,2), V(:,3));
    out = 2*sum(tf_joint)/(sum(tf1)+sum(tf2));
end

% there are 2 overlap functions, the first one is some estimation of the
% actual overlap value.
function out = overlap(pc1,pc2)
    % this is the function that estimates overlap between 2 point clouds
    xx1 = pc1.XLimits;
    yy1 = pc1.YLimits;
    zz1 = pc1.ZLimits;
    xx2 = pc2.XLimits;
    yy2 = pc2.YLimits;
    zz2 = pc2.ZLimits;
    
    a1 = mean(xx1);
    a2 = mean(xx2);
    if(a1>a2)
        temp = xx1;
        xx1 = xx2;
        xx2 = temp;
    end
    
    a1 = mean(yy1);
    a2 = mean(yy2);
    if(a1>a2)
        temp = yy1;
        yy1 = yy2;
        yy2 = temp;
    end
    
    a1 = mean(zz1);
    a2 = mean(zz2);
    if(a1>a2)
        temp = zz1;
        zz1 = zz2;
        zz2 = temp;
    end
    
    xo = xx1(2) - xx2(1);
    yo = yy1(2) - yy2(1);
    zo = zz1(2) - zz2(1);
    if (xo <0)
        xo = 0;
    end

    if (yo <0)
        yo = 0;
    end
    
    if (zo <0)
        zo = 0;
    end
    out = xo*yo*zo;   
end
function out = myOverlap2(v1, v2)
    over = alphaShape(v1(:,1), v1(:,2), v1(:,3)); 
    out = sum(inShape(over,v2(:,1), v2(:,2), v2(:,3)));
end

function [pc,Apc,mpc] = pdCPD(SL , ASL , ratio)
    % CPD for  cord
    pc = pointCloud(SL);
    pc = pcdownsample(pc,'random',ratio);
    Apc = pointCloud(ASL);
    Apc = pcdownsample(Apc,'random',ratio);

    %icp
    [tform1,mpc] = pcregistericp(pc,Apc);
%     mpc = pctransform(pc,tform1);
    
    %cpd
    [tform2,mpc] = pcregistercpd(mpc,Apc);
%     mpc = pctransform(mpc,tform2);
    
    figure()
    pcshowpair(mpc,Apc,'MarkerSize',50)
    title('Point clouds after registration')
    legend({'Moving point cloud','Fixed point cloud'},'TextColor','w')
    legend('Location','southoutside')
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
end

function [tform,mpc] = pdCPDTrans(SL , ASL , ratio)
    % CPD for  cord
    pc = pointCloud(SL);
    pc = pcdownsample(pc,'random',ratio);
    Apc = pointCloud(ASL);
    Apc = pcdownsample(Apc,'random',ratio);
    %cpd
    [tform,mpc] = pcregistercpd(pc,Apc);
%     mpc = pctransform(mpc,tform2);
end

function myPlot(V, M, name)
    % part one
    [xx yy zz] = size(M);
    % 1
    clear c X;
    nn = floor(xx/2);
    X(: , :) = V(nn,:,:); 
    X = mat2gray(X);
    J2 = (cat(3, X, X, X));

    c(: , :) = M(nn,:,:); 
    J1 = 256 * repmat(uint8(c), 1,1, 3);
    J1(: , : , 3) = 0;
    J1 = double(J1);
    o1 = J1+J2;
    % imshow(J1+J2);

    % 2
    clear c X;
    nn = floor(yy/2);
    X(: , :) = V(:,nn,:); 
    X = mat2gray(X);
    J2 = (cat(3, X, X, X));

    c(: , :) = M(:,nn,:); 
    J1 = 256 * repmat(uint8(c), 1,1, 3);
    J1(: , : , 3) = 0;
    J1 = double(J1);
    o2 = J1+J2;

    % 3
    clear c X;
    nn = floor(zz/2);
    X(: , :) = V(:,:,nn); 
    X = mat2gray(X);
    J2 = (cat(3, X, X, X));

    c(: , :) = M(:,:,nn); 
    J1 = 256 * repmat(uint8(c), 1,1, 3);
    J1(: , : , 3) = 0;
    J1 = double(J1);
    o3 = J1+J2;

    figure()
    subplot(1 , 3, 1)
    imshow(o1)
    subplot(1 , 3, 2)
    imshow(o2)
    title("frontal, axial and lateral cuts");
    xlabel(name);
    subplot(1 , 3, 3)
    imshow(o3)
end

function out = myJaccob(ddx,ddy,ddz)
    % Jaccobian matrix output
    [gxy,gxx,gxz] = gradient(ddx);
    [gyy,gyx,gyz] = gradient(ddy);
    [gzy,gzx,gzz] = gradient(ddz);
    gxx = gxx + 1;
    gyy = gyy + 1;
    gzz = gzz + 1;   
    J = gxx.*gyy.*gzz + gyx.*gzy.*gxz + gzx.*gxy.*gyz - gzx.*gyy.*gxz - gyx.*gxy.*gzz - gxx.*gzy.*gyz;
    out = length(find(J<0));
end

% an online code I found for hoddorf distance is given below, because my own
% code takes too much time to run:
% %%% ZCD Oct 2009 %%%
function [hd] = HausdorffDist(P,Q,lmf)

sP = size(P); 
sQ = size(Q);
if ~(sP(2)==sQ(2))
    error('Inputs P and Q must have the same number of columns')
end
if nargin > 2 && ~isempty(lmf)
    % the user has specified the large matrix flag one way or the other
    largeMat = lmf;     
    if ~(largeMat==1 || largeMat==0)
        error('3rd ''lmf'' input must be 0 or 1')
    end
else
    largeMat = 0;   % assume this is a small matrix until we check
    % If the result is too large, we will not be able to build the matrix of
    % differences, we must loop.
    if sP(1)*sQ(1) > 2e6
        % ok, the resulting matrix or P-to-Q distances will be really big, lets
        % check if our memory can handle the space we'll need
        memSpecs = memory;          % load in memory specifications
        varSpecs = whos('P','Q');   % load in variable memory specs
        sf = 10;                    % build in a saftey factor of 10 so we don't run out of memory for sure
        if prod([varSpecs.bytes]./[sP(2) sQ(2)]) > memSpecs.MaxPossibleArrayBytes/sf
            largeMat = 1;   % we have now concluded this is a large matrix situation
        end
    end
end
if largeMat

maxP = 0;           % initialize our max value
% loop through all points in P looking for maxes
for p = 1:sP(1)
    % calculate the minimum distance from points in P to Q
    minP = min(sum( bsxfun(@minus,P(p,:),Q).^2, 2));
    if minP>maxP
        % we've discovered a new largest minimum for P
        maxP = minP;
    end
end
% repeat for points in Q
maxQ = 0;
for q = 1:sQ(1)
    minQ = min(sum( bsxfun(@minus,Q(q,:),P).^2, 2));
    if minQ>maxQ
        maxQ = minQ;
    end
end
hd = sqrt(max([maxP maxQ]));
D = [];
    
else
iP = repmat(1:sP(1),[1,sQ(1)])';
iQ = repmat(1:sQ(1),[sP(1),1]);
combos = [iP,iQ(:)];

cP=P(combos(:,1),:); cQ=Q(combos(:,2),:);
dists = sqrt(sum((cP - cQ).^2,2));

D = reshape(dists,sP(1),[]);

vp = max(min(D,[],2));

vq = max(min(D,[],1));
hd = max(vp,vq);
end
end
