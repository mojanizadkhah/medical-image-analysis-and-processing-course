%% MIAP HW4
%% Q1-1
clc;
clear;
im1 = imread("Mri1.bmp");
im2 = imread("Mri2.bmp");
im3 = imread("Mri3.bmp");
im4 = imread("Mri4.bmp");
im5 = imread("Mri5.bmp");

%% Q1-1
a = cat(3,im4,im2,im3);
b = cat(3,im1,im2,im5);
c = cat(3,im3,im2,im1);
figure();
subplot(1 , 3 , 1)
imshow(a);
subplot(1 , 3 , 2)
imshow(b);
title("Q1 oart1");
subplot(1 , 3 , 3)
imshow(c);

%% Q1-2 soft clustering
clc;
clear;
I = imread("Mri1.bmp"); 
[m, n] = size(I);
data = zeros(m*n,1);
data(1:m*n,1) = reshape(double(I), m*n,1);
 
Nc= 5;
options = [1.4, NaN, NaN, 0];
[center, U] = fcm(data, Nc, options);
U1 = (reshape(U,[5 , m,n]));

figure();
subplot(1 , Nc+1 , 1)
imshow(I);
xlabel("original picture");
% ylabel('soft clustering with q = 1.4');
for i = 2:Nc+1
    x(: , :) = U1(i-1 , : , :);
    subplot(1 , Nc+1 , i)
    imshow(x);
    xlabel(string("cluster = "+ num2str(i-1)));
    
end


%% Q1-2 hard clustering
clc;
clear;
I = imread("Mri1.bmp"); 
[m, n] = size(I);
data = zeros(m*n,1);
data(1:m*n,1) = reshape(double(I), m*n,1);
 
Nc= 5;
options = [5, NaN, NaN, 0];
[center, U] = fcm(data, Nc);
U1 = (reshape(U,[5 , m,n]));

figure();
subplot(1 , Nc+1 , 1)
imshow(I);
xlabel("original picture");
% ylabel('hard clustering');
for i = 2:Nc+1
    x(: , :) = U1(i-1 , : , :);
    subplot(1 , Nc+1 , i)
    imshow(x);
    xlabel(string("cluster = "+ num2str(i-1)));
    
end

%% Q1_3

[id,centers_kmeans] = kmeans(data,Nc);
c(1,: , :) = reshape((id==1),[m,n]);
c(2,: , :) = reshape((id==2),[m,n]);
c(3,: , :) = reshape((id==3),[m,n]);
c(4,: , :) = reshape((id==4),[m,n]);
c(5,: , :) = reshape((id==5),[m,n]);

figure();
subplot(1 , Nc+1 , 1)
imshow(I);
xlabel("original picture");
ylabel('k means way(Q1 part3)');
for i = 2:Nc+1
    x(: , :) = c(i-1 , : , :);
    subplot(1 , Nc+1 , i)
    imshow(x);
    xlabel(string("cluster = "+ num2str(i-1)));
    
end


subplot(1 , Nc+1 , 1)
imshow(I);
xlabel("original picture");
ylabel('FCM (Q1 part3)');
for i = 2:Nc+1
    x(: , :) = c(i-1 , : , :);
    subplot(1 , Nc+1 , i)
    imshow(x);
    xlabel(string("cluster = "+ num2str(i-1)));
    
end

%% Q1_4
clc;

im1 = imread("Mri1.bmp");
im2 = imread("Mri2.bmp");
im3 = imread("Mri3.bmp");
im4 = imread("Mri4.bmp");
im5 = imread("Mri5.bmp");
Tot = zeros(m*n,5);
Tot(: , 1) = reshape(im1 , m*n , 1);
Tot(: , 2) = reshape(im2 , m*n , 1);
Tot(: , 3) = reshape(im3 , m*n , 1);
Tot(: , 4) = reshape(im4 , m*n , 1);
Tot(: , 5) = reshape(im5 , m*n , 1);

GMModel = fitgmdist(Tot,Nc,'RegularizationValue',0.01);
[id] = cluster(GMModel,Tot);
for i = 1:5
	c(i , : , :) = reshape((id==i),[m,n]);
end

figure();
subplot(1 , Nc+1 , 1)
imshow(I);
xlabel("original picture");
ylabel('GMM model,Q1 part4');
for i = 2:Nc+1
    x(: , :) = c(i-1 , : , :);
    subplot(1 , Nc+1 , i)
    imshow(x);
    xlabel(string("cluster = "+ num2str(i-1)));
    
end



%% Q1_5

new = zeros(m*n,1);
cc = sort(U,'descend');
U5 = U>0.7;
U5 = (reshape(U5,[5 , m,n]));
figure();
for i = 1:Nc
    x(: , :) = U5(i , : , :);
    subplot(1 , Nc , i)
    imshow(x);
    xlabel(string("cluster = "+ num2str(i)));
    if(i==3)
        title("Q1 part 5: partial volume");
    end   
end

%% Q2-1
clc;
clear;
im1 = imread("Blur1.png");
im2 = imread("Blur2.png");
% GVF 2-1
[u,v] = GVF(im1, 0.2, 20);
figure()
imshow(im1);
title("Blur1 with GVF(mu = 0.2 , iters = 20)");
hold on;
quiver(u,v);
axis ij off;
hold off

figure();
[u,v] = GVF(im2, 0.2, 20);

imshow(im2);
title("Blur2 with GVF(mu = 0.2 , iters = 20)");
hold on;
quiver(u,v);
axis ij off;
%% GVF 2-2

im(1 , : , :) = imread("Mri1.bmp");
im(2 , : , :) = imread("Mri2.bmp");
im(3 , : , :) = imread("Mri3.bmp");
im(4 , : , :) = imread("Mri4.bmp");
im(5 , : , :) = imread("Mri5.bmp");

figure();

for i = 1:5
    a(: , :) = im(i , : , :);
    [u,v] = GVF(a, 0.2, 20);
    subplot(1 , 5 , i);
    imshow(a);    
    hold on;
    quiver(u,v);
    axis ij off;
    hold off
    if(i==3)
        title("GVF MRI(mu = 0.2 , iters = 20)");
    end
end
%% snake 2-2 & 2-1
% converting the format of picture for snake to ba able to read them
for i = 1:5
    a(:,:) = im(i , : , :);
    k = num2str(i)+".tif";
    imwrite(a, k);
end
%% Q3-3
% the demo file
clear all
close all
clc
winSizeo=[1 3 5 7 9 11 13 15];

for i=1:8
load('noise.mat')
%%%%%% Initialization %%%%%%%%%

winSize=winSizeo(i);          % Size of the local window
cNum=6;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"


%%%%% Uncomment the desired image to run the experiment (for more details, plz refer to the paper)

% img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity
% img=no720_100S;      % Sagital slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity
img=rice10_91A;       % Axial slice no. 91 corrupted with 10% Rician noise

% img=Brats1;          % Slices no. 80 from pat266_1 (Brats challenge 2014) 
% img=Brats2;          % Slices no. 86 from pat192_1 (Brats challenge 2014) 


[r,c]=size(img);
img=double(img);

%%% Function calls %%%%

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

%%% Defuzzification process

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);

%%% Note: the appearance of the segmented image may look different due to
%%% the order of the label values

titl=strcat('Segmentation with ' , ' "', opt, '" filtered image');
figure(5),
% subplot(121),imshow(img,[]), title('Input Image');
subplot(2,4,i),imshow(segment,[]);%title(titl)

clear r c cNum mMax opt t w winSize
end;




%% Q3-4
% investigating the effect of cluster number
% I coppied the demo file to change it accordingly
clc;
clear;
winSizeo=[1 3 5 7 9 11 13 15];
%cNum = 3
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=3;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
seg3 = segment;
clear r c cNum mMax opt t w winSize
end
%cNum = 8
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=8;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
seg8 = segment;
clear r c cNum mMax opt t w winSize
end
%cNum = 13
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=13;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
seg13 = segment;
clear r c cNum mMax opt t w winSize
end


figure()
subplot(1,4,1)
imshow(uint8(no720_100A));
xlabel("the picture");

subplot(1,4,2)
imshow(seg3,[]);
xlabel("cNum=3");

subplot(1,4,3)
imshow(seg8,[]);
xlabel("cNum=8");

subplot(1,4,4)
imshow(seg13,[]);
xlabel("cNum=13");


%cNum = 4
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=4;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
seg4 = segment;
clear r c cNum mMax opt t w winSize
end

figure()
imshow(seg4,[]);
xlabel("cNum=4");

%% Q3-5
% opt = average
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=6;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
segavg = segment;
clear r c cNum mMax opt t w winSize
end

% opt = med
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=6;             % Number of clusters
opt='median';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
segmed = segment;
clear r c cNum mMax opt t w winSize
end

% opt = wei
for i=1:8
load('noise.mat')
winSize=winSizeo(i);          % Size of the local window
cNum=6;             % Number of clusters
opt='weighted';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
segwei = segment;
clear r c cNum mMax opt t w winSize
end


figure()
subplot(1,4,1)
imshow(uint8(no720_100A));
xlabel("the picture");

subplot(1,4,2)
imshow(segavg,[]);
xlabel("opt = average");

subplot(1,4,3)
imshow(segmed,[]);
xlabel("opt = median");

subplot(1,4,4)
imshow(segwei,[]);
xlabel("opt = weighted");

%% Q3-6
% winsize = 1
load('noise.mat')
winSize=1;          % Size of the local window
cNum=6;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
segw1 = segment;
clear r c cNum mMax opt t w winSize


% winsize = 29
load('noise.mat')
winSize=29;          % Size of the local window
cNum=6;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
segw29 = segment;
clear r c cNum mMax opt t w winSize

% winsize = 15
load('noise.mat')
winSize=15;          % Size of the local window
cNum=6;             % Number of clusters
opt='average';      % Filtered image version "average", "median", or "weighted"
img=no720_100A;       % Axial slice no. 100 corrupted with 7% noise and 20% grayscale non-uniformity

[r,c]=size(img);
img=double(img);

w=PixWgt(img,winSize);
segment=ARKFCM(img,w,opt,cNum);

[mMax,segment]=max(segment.U,[],2);
segment=reshape(segment,r,c);
segw15 = segment;
clear r c cNum mMax opt t w winSize


figure();
subplot(1,3,1)
imshow(segw1,[]);
xlabel("window size = 1");


subplot(1,3,2)
imshow(segw15,[]);
xlabel("window size = 15");

subplot(1,3,3)
imshow(segw29,[]);
xlabel("window size = 29");
