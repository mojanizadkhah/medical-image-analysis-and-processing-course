%% Q1
clc;
clear;
x = imread("city_noise.jpg");
x = im2double(x);
y = imread("city_orig.jpg");
y = im2double(y);
[h , w] = size(x);

snrRU = mySNR(x(1:h/2 , 1:w/2) , y(1:h/2 , 1:w/2));
snrRD = mySNR(x(h/2+1:h , 1:w/2) , y(h/2+1:h , 1:w/2));
snrLU = mySNR(x(1:h/2 , w/2+1:w) , y(1:h/2 , w/2+1:w));
snrLD = mySNR(x(h/2+1:h , w/2+1:w) , y(h/2+1:h , w/2+1:w));



h1 = ones(5)/sum(ones(5));
Bmean = imfilter(x,h1);
Bmed = medfilt2(x);
Bgaus = imgaussfilt(x);

%Bmean
snrRUmean = mySNR(Bmean(1:h/2 , 1:w/2) , y(1:h/2 , 1:w/2));
snrRDmean = mySNR(Bmean(h/2+1:h , 1:w/2) , y(h/2+1:h , 1:w/2));
snrLUmean = mySNR(Bmean(1:h/2 , w/2+1:w) , y(1:h/2 , w/2+1:w));
snrLDmean = mySNR(Bmean(h/2+1:h , w/2+1:w) , y(h/2+1:h , w/2+1:w));

%Bmed
snrRUmed = mySNR(Bmed(1:h/2 , 1:w/2) , y(1:h/2 , 1:w/2));
snrRDmed = mySNR(Bmed(h/2+1:h , 1:w/2) , y(h/2+1:h , 1:w/2));
snrLUmed = mySNR(Bmed(1:h/2 , w/2+1:w) , y(1:h/2 , w/2+1:w));
snrLDmed = mySNR(Bmed(h/2+1:h , w/2+1:w) , y(h/2+1:h , w/2+1:w));


%Bgaus
snrRUgaus = mySNR(Bgaus(1:h/2 , 1:w/2) , y(1:h/2 , 1:w/2));
snrRDgaus = mySNR(Bgaus(h/2+1:h , 1:w/2) , y(h/2+1:h , 1:w/2));
snrLUgaus = mySNR(Bgaus(1:h/2 , w/2+1:w) , y(1:h/2 , w/2+1:w));
snrLDgaus = mySNR(Bgaus(h/2+1:h , w/2+1:w) , y(h/2+1:h , w/2+1:w));


% the whole picture
snr_mean = mySNR(Bmean , y);
snr_med = mySNR(Bmed , y);
snr_gaus = mySNR(Bgaus , y);
snr = mySNR(x , y);


figure()
subplot(2 , 2 , 1);
imshow((Bmean));
xlabel("after mean, SNR = "+ num2str(snr_mean));

subplot(2 , 2 , 2);
imshow((Bmed));
xlabel("after median, SNR = "+ num2str(snr_med));

subplot(2 , 2 , 3);
imshow((Bgaus));
xlabel("after Gaussian, SNR = "+ num2str(snr_gaus));

subplot(2 , 2 , 4);
imshow((y));
xlabel("original image= "+ num2str(snr));

%% Q2
clc;
clear;
im = double(imread("hand_xray.jpg"));

figure()
subplot(1 , 3, 1)
fftim = abs(fftshift(fft2(im)));
imshow(uint8(fftim));
xlabel("fft img");

subplot(1 , 3, 2)
logfftim=log(abs(fftshift(fft2(im))));

imshow(mat2gray(uint8(logfftim)));
xlabel("log fft img");

subplot(1 , 3, 3)
imagesc(mat2gray(uint8(logfftim)));
xlabel("log fft using imagesc");

%% Q2 partb
[h , w] = size(im);
Pim = sum(sum(im))/(w*h);
Pfftim = sum(sum(fftim(end/2+1,end/2+1)))/(w*h);
%% Q3 part a
clc;
clear;
im = im2double(imread("chessboard.jpg"));

h1 = [1 , -1];
h2 = [1 , 0];
h3 = [1 , 0 , -1];
h4 = [1 , 0 , -1]';
h5 = [-1,-1,-1;-1,8,-1;-1,-1,-1];

im1 = imfilter(im,h1);
im2 = imfilter(im,h2);
im3 = imfilter(im,h3);
im4 = imfilter(im,h4);
im5 = imfilter(im,h5);

figure();
imshow(im1);
title("after filter [1 , -1]");

figure();
imshow(im2);
title("after filter [1 , 0]");

figure();
imshow(im3);
title("after filter [1 , 0 , -1]");

figure();
imshow(im4);
title("after filter [1 , 0 , -1]'");

figure();
imshow(im5);
title("after filter [-1,-1,-1;-1,8,-1;-1,-1,-1]");
%% Q3 part b
clc;
clear;
im = rgb2gray(imread("chessboard.jpg"));

BW1 = edge(im,'sobel');
BW2 = edge(im,'canny');
BW3 = edge(im,'loG');

figure()
subplot(2 , 2 , 1);
imshow(im);
xlabel("original");

subplot(2 , 2 , 2);
imshow(BW1);
xlabel("sobel");

subplot(2 , 2 , 3);
imshow(BW2);
xlabel("canny");

subplot(2 , 2 , 4);
imshow(BW3)
xlabel("laplacian of gaussian");
%% Q4
clc;
clear;
%a1=log(1+abs(fftshift(fft2(a))));
im = double(imread("hand_xray.jpg"));
fftim = fftshift(fft2(im));
[h , w] = size(im);
t = abs(h - (1:h))+1;
fftnew = fftim(t,:);
% for m = 1:h
% fftnew(m , :) = fftim(m ,:).*exp(1i*2*pi*(h-m));
% 
% end
figure()
subplot(1 , 2 , 1);
imshow(uint8(im));
xlabel("original");
subplot(1 , 2 , 2);

imnew = abs(ifft2(fftnew));
imshow(uint8(imnew));
xlabel("img after rotation with fft");

%% Q5
clc;
clear;
im1 = (imread("hand_xray.jpg"));
im2 = (imread("brain_xray.jpg"));

fim1 = fft2(im1);
fim2 = fft2(im2);

new1 = uint8(ifft2(abs(fim1).*exp(1i*angle(fim2))));
new2 = uint8(ifft2(abs(fim2).*exp(1i*angle(fim1))));


figure()
subplot(1, 2, 1)
imshow(new1);
xlabel("abs hand-xray + phase head-xray");

subplot(1, 2, 2)
imshow(new2);
xlabel("abs head-xray + phase hand-xray");

%% Q6 
% part a
clc;
clear;
img = rgb2gray(imread("wall.jpg"));
fftimg=fftshift(fft2(img));
% IF=ifft2(fftshift(fftimg));

figure()
subplot(1 , 2, 1);
imshow(img)
xlabel("image");

subplot(1 , 2, 2);

imagesc(log(abs(fftimg)))
xlabel("fft image");
%% Q6 part b
out5 = myLPF(5 , img); 
out10 = myLPF(10 , img);
out15 = myLPF(15 , img);
out20 = myLPF(20 , img);
figure()
subplot(2 , 2, 1);
imshow(uint8(out5));
xlabel("Th = 5");

subplot(2 , 2, 2);
imshow(uint8(out10));
xlabel("Th = 10");

subplot(2 , 2, 3);
imshow(uint8(out15));
xlabel("Th = 15");

subplot(2 , 2, 4);
imshow(uint8(out20));
xlabel("Th = 20");
%% Q6 part c
out5 = myHPF(5 , img); 
out10 = myHPF(10 , img);
out15 = myHPF(15 , img);
out20 = myHPF(20 , img);
figure()
subplot(2 , 2, 1);
imshow(uint8(out5));
xlabel("Th = 5");

subplot(2 , 2, 2);
imshow(uint8(out10));
xlabel("Th = 10");

subplot(2 , 2, 3);
imshow(uint8(out15));
xlabel("Th = 15");

subplot(2 , 2, 4);
imshow(uint8(out20));
xlabel("Th = 20");
%% Q6 part d (gaussian)
% h = fspecial('gaussian' , 10  , 10)
% because of recommendation of matlab, I used imgaussfilt instead.
outlpf = imgaussfilt(img , 5);
outhpf = double(img - outlpf);

figure();
subplot(1 , 2 , 1)
imshow(outlpf);
xlabel("LPF gaussian");
subplot(1 , 2 , 2)
imshow(outhpf);
xlabel("HPF gaussian");

%% Q6 part d (laplacian)
h = fspecial('laplacian',0.4);

outlpf = imfilter(img , h);
outhpf = double(img - outlpf);

figure();
subplot(1 , 2 , 1)
imshow(uint8(outlpf));
xlabel("LPF laplacian");
title("alfa = 0.4");
subplot(1 , 2 , 2)
imshow(uint8(outhpf));
xlabel("HPF laplacian");
title("alfa = 0.4");

%% Q6 part d (butter)

outlpf = butter(double(img) , 4 , 20);
outhpf = double(double(img) - outlpf);

figure();
subplot(1 , 2 , 1)
imshow(uint8(outlpf));
xlabel("LPF butterworth");
title("(F = 20) , order = 4");
subplot(1 , 2 , 2)
imshow((outhpf));
xlabel("HPF butterworth");
title("(F = 20) , order = 4");
%% functions
function out = mySNR(x , y)
    a = sum((x.^2));
    b = sum(((x-y).^2 ));
    out = 10*log10(a/b);
end

function out = myLPF(M , img)
    % M is the threshhold for filtering in frequency domain
    fftimg=fftshift(fft2(img));
    [h , w] = size(fftimg);
    
    % filtering in frequency mode
    newfftimg = fftimg;
    A = ones(h , w);
    A(end/2-M+1:M+end/2+1 , end/2-M+1:M+end/2+1) = 0;
    A = 1-A;
    newfftimg = A.* newfftimg;

    IF=abs(ifft2(fftshift(newfftimg)));
    out = IF;
end

function out = myHPF(M , img)
    % M is the threshhold for filtering in frequency domain
    fftimg=fftshift(fft2(img));
    [h , w] = size(fftimg);
    
    % filtering in frequency mode
    newfftimg = fftimg;
    A = ones(h , w);
    A(end/2-M+1:M+end/2+1 , end/2-M+1:M+end/2+1) = 0;
    newfftimg = A.* newfftimg;

    IF=abs(ifft2(fftshift(newfftimg)));
    out = IF;
end

 
function out = butter(img , n , D0)
[M, N] = size(img);
FT_img = fft2(double(img));

% Designing filter
u = 0:(M-1);
v = 0:(N-1);
idx = find(u > M/2);
u(idx) = u(idx) - M;
idy = find(v > N/2);
v(idy) = v(idy) - N;
[V, U] = meshgrid(v, u);
D = sqrt(U.^2 + V.^2);
H = 1./(1 + (D./D0).^(2*n));
G = H.*FT_img;
out = real(ifft2(double(G))); 

% cite from :https://www.geeksforgeeks.org/
%matlab-butterworth-lowpass-filter-in-image-processing/
end