%% MIAP HW3
%% Q1 part a
clc;
clear;

F = phantom('Modified Shepp-Logan',500);
G = imnoise(F,'gaussian',0,(0.05).^2);

figure();
subplot(1 , 2 ,1)
imshow(F);
title("image");
subplot(1 , 2 ,2)
imshow(G);
title("noisy image");
%% Q1 part b

Wlen = 3; 
Wsearch = 5; 
hv = 0.1;   
mu = 0.1;

out = myNLM(G , Wlen , Wsearch , hv , mu);
figure();
subplot(1 , 3 ,1)
imshow(F);
title("image");
subplot(1 , 3 ,2)
imshow(G);
title("noisy image");
subplot(1 , 3 ,3)
imshow(out);
title("image after NLM");
%% Q1 part c
SNRnoise = mySNR(F,G);
SNRfiltNLM = mySNR(F,out);

EPInoise = EPI(F, G);
EPIfiltNLM = EPI(F, out);
%% Q2 part b & c
clc;
clear;

F = phantom('Modified Shepp-Logan',500);
G = imnoise(F,'gaussian',0,(0.05).^2);
Wlen = 9; 
hg = 0.15; 
hx = 6;
out = myBilateral(G , Wlen , hg , hx);

figure();
subplot(1 , 3 ,1)
imshow(F);
title("image");
subplot(1 , 3 ,2)
imshow(G);
title("noisy image");
subplot(1 , 3 ,3)
imshow(out);
title("image after bilateral");


SNRnoise = mySNR(F,G);
SNRfiltBL = mySNR(F,out);

EPInoise = EPI(F, G);
EPIfiltBL = EPI(F, out);
%% Q3 a
clc;
clear;

F = phantom('Modified Shepp-Logan',500);
G = imnoise(F,'gaussian',0,(0.05).^2);

iterations = 100;
dt = 0.01;
landa = 10;

out = myTV(G , iterations , dt , landa);

figure();
subplot(1 , 3 ,1)
imshow(F);
title("image");
subplot(1 , 3 ,2)
imshow(G);
title("noisy image");
subplot(1 , 3 ,3)
imshow(out);
title("image after Total Variation");


SNRnoise = mySNR(F,G);
SNRfiltTV = mySNR(F,out);

EPInoise = EPI(F, G);
EPIfiltTV = EPI(F, out);
%% functions

function out = mySNR(x,y)
    out = 10 * log10(sum(sum(x.^2))/sum(sum((x-y).^2)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% this is for calculating eucledean distance
function out = dist(a , b)
    [w , h] = size(a);
    m = posKernel(w)*(a-b).^2;
    out = sum(sum(m))/(w*h);
end

function out = myKernel(a , b , hv)
    out = exp(- ( dist(a , b)/(2*hv^2) ) );
end

% this is for positional value
function out = posKernel(Wlen)               
    kernel=zeros(Wlen,Wlen); 
    wl = (Wlen-1)/2;
    for l=0:wl    
        value= 1 / (2*l+1)^2 ;    
        for i=-l:l
        for j=-l:l
        kernel(wl+1-i,wl+1-j) = kernel(wl+1-i,wl+1-j) + value ;
        end
        end
    end
    out = kernel ./ sum(sum(kernel));
end    

function out = myNLM(G , Wlen , Wsearch , hv , mu)
    % G : the noisy picture
    % Wlen : length similarity window
    % Wsearch : length search window
    % hv : component for noise
    % mu : component for importance of each picxel in its output
    % after more considarations, I eliminated mu and replaced it with 
    % maxpic which was intrudiuced in the slides and the original article

    wl = (Wlen-1)/2;
    ws = (Wsearch-1)/2;


    [width , height] = size(G);
    out = zeros(width , height);

    G1 = padarray(G,[wl wl],'symmetric');

    for i = 1+ wl : width + wl
    for j = 1+ wl : height + wl        
        mdist = zeros(Wlen);
        meanweight = 0;
        Sx = G1(i-wl: i+wl , j-wl: j+wl);
        maxpic = 0;
        wmax = min(i+ws,width+wl);
        wmin = max(i-ws,wl+1);
        hmax = min(j+ws,height+wl);
        hmin = max(j-ws,wl+1);

        for k = wmin:wmax
        for t = hmin:hmax
            Sy = G1(k-wl: k+wl , t-wl: t+wl);
            if(k==i && t==j)
                continue;
            end
            a = myKernel(Sx , Sy , hv);
            mdist = mdist + a*(Sy(1+wl , 1+wl));
            meanweight = meanweight + a; 
            if(a>maxpic)
                maxpic = a;
            end
        end
        end
        
        if(meanweight >0)
            out(i-wl , j-wl) = (maxpic*G(i-wl , j-wl)+mdist(wl+1 , wl+1))/(meanweight+maxpic);
%             out(i-wl , j-wl) = (mu*G(i-wl , j-wl)+mdist(wl+1 , wl+1))/(meanweight+mu);
        else
            out(i-wl , j-wl) = G(i-wl , j-wl);
        end
    end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = Ghg(a , hg)
    [w , h] = size(a);
    center = a((w+1)/2 ,(h+1)/2);
    m = (a - center).^2;
    out = exp(- ( m/(2*hg^2) ) );
%     out = out/sum(sum(out));
end


function out = Ghx(Wlen , hx)               
    out = zeros(Wlen,Wlen); 
    wl = (Wlen-1)/2;
    for l=0:wl     
        for i=-l:l
        for j=-l:l
            out(wl+1-i,wl+1-j) = exp(- ( (i^2+j^2)/(2*hx.^2) )) ;
        end
        end
    end
%     out = out/sum(sum(out));
end


function out = myBilateral(G , Wlen , hg , hx)
    % G : the noisy picture
    % Wlen : length similarity window
    % hv : component for light diffrence
    % hx : component for distance
    wl = (Wlen-1)/2;

    [width , height] = size(G);
    out = zeros(width , height);
    
    G1 = G - mean(mean(G));
    G1 = padarray(G1,[wl wl],'symmetric');

    Gx = Ghx(Wlen , hx);
    for i = 1+ wl : width + wl
    for j = 1+ wl : height + wl        
        Sx = G1(i-wl: i+wl , j-wl: j+wl);
                
        Gg = Ghg(Sx, hg);
        GG = Gx.*Gg;
        out(i-wl , j-wl) = sum(sum(Sx.*GG))/sum(sum(GG));      
    end
    end
    out = out+ mean(mean(G));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the total variation function
function out = myTV(G , iterations , dt , landa)
    % G : the noisy picture
    % iterations : number of iteration
    % dt : time parameter
    epsilon = 0.0001;
    meanG = mean(G , 'all');
    u = G - meanG;
    f = u;
    for iter = 1:iterations
        [upx, upy]=graddp(u);
        [umx, umy]=graddm(u);
        xx = upx./sqrt(upx.^2 + mm(upy , umy).^2 + epsilon);
        yy = upy./sqrt(upy.^2 + mm(upx , umx).^2 + epsilon);
        [uxx , ~] = graddm(xx);
        [~ , uyy] = graddm(yy);
        u = u + dt*(uxx + uyy) + dt*landa*(f - u);   
    end
    out = u + meanG;
end

%prerequisite functions the myTV function
function out = mm(a , b)
    out = ((sign(a)+sign(b))/2)*min(abs(a) , abs(b));
end

function [ux , uy] = graddp(u)
    [w , h] = size(u);
    ux=u([2:w, w], :) - u;
    uy=u(:, [2:h,h]) - u;   
end

function [ux , uy] = graddm(u)
    [w , h] = size(u);
    ux=u - u([1,1:w-1], :);
    uy=u - u(:, [1,1:h-1]);
end
  